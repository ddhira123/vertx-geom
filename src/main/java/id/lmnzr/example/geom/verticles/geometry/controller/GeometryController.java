package id.lmnzr.example.geom.verticles.geometry.controller;

import id.lmnzr.example.geom.verticles.geometry.GeometryConfig;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * The type Geometry controller.
 */
public class GeometryController {
    private static final String FILTER_ID = "id";
    private static final String FILTER_NAME = "name";
    private static final String FILTER_LIMIT = "limit";
    private static final String FILTER_OFFSET = "offset";

    private final Logger log = LoggerFactory.getLogger(GeometryController.class);

    private final Vertx vertx;

    /**
     * Instantiates a new Geometry controller.
     *
     * @param vertx the vertx
     */
    public GeometryController(Vertx vertx) {
        this.vertx = vertx;
    }

    /**
     * Gets router.
     *
     * @return the router
     */
    public Router getRouter() {
        Router router = Router.router(this.vertx);
        router.route("/").handler(BodyHandler.create());
        router.get("/").handler(this::retrieveHandler);
        router.post("/").handler(this::saveHandler);
        router.delete("/").handler(this::deleteHandler);
        router.get("/health").handler(this::healthHandler);
        return router;
    }

    private void healthHandler(RoutingContext context) {
        String message = "health check";
        publishEventbus(context, GeometryConfig.HEALTH, message);
    }

    private void retrieveHandler(RoutingContext context) {
        MultiMap params = context.request().params();
        if (params.contains(FILTER_ID)) {
            publishEventbus(context, GeometryConfig.FIND_BY_ID, params.get(FILTER_ID));
        } else if (params.contains(FILTER_NAME)) {
            publishEventbus(context, GeometryConfig.FIND_BY_NAME, params.get(FILTER_NAME));
        } else {
            Integer limit = params.contains(FILTER_LIMIT) ? Integer.parseInt(params.get(FILTER_LIMIT)) : null;
            Integer offset = params.contains(FILTER_OFFSET) ? Integer.parseInt(params.get(FILTER_OFFSET)) : null;

            JsonObject jsonReq = new JsonObject();
            jsonReq.put(FILTER_LIMIT, limit);
            jsonReq.put(FILTER_OFFSET, offset);

            publishEventbus(context, GeometryConfig.FETCH, jsonReq.encode());
        }
    }

    private void saveHandler(RoutingContext context) {
        JsonObject requestJson = getRequestContent(context);
        publishEventbus(context, GeometryConfig.SAVE, requestJson.encode());
    }

    private void deleteHandler(RoutingContext context) {
        MultiMap params = context.request().params();
        if (params.contains(FILTER_ID)) {
            publishEventbus(context, GeometryConfig.DELETE, params.get(FILTER_ID));
        } else{
            errorHandler(context,400,"Bad Request","no geometry id");
        }
    }

    private void successHandler(RoutingContext context, String responseBody) {
        context.response().setStatusCode(200)
            .setStatusMessage("Ok")
            .putHeader("content-type", "application/json; charset=utf-8")
            .end(responseBody);
    }

    private void errorHandler(RoutingContext context, int statusCode, String statusMessage, String errorMessage) {
        context.response().setStatusCode(statusCode)
            .setStatusMessage(statusMessage)
            .putHeader("content-type", "application/json; charset=utf-8")
            .end(Json.encodePrettily(new JsonObject()
                .put("success", false)
                .put("error", errorMessage)
            ));
    }

    private void publishEventbus(RoutingContext context, String ebAddress, String message) {
        vertx.eventBus().request(ebAddress, message, handler -> {
            if (handler.succeeded()) {
                logSuccessPublishing(ebAddress, handler.result().body().toString());
                successHandler(context, handler.result().body().toString());
            } else {
                logFailPublishing(ebAddress, handler.cause().getMessage());
                errorHandler(context, 500, "Server Error", handler.cause().getMessage());
            }
        });
    }

    private void logSuccessPublishing(String address, String message) {
        log.debug("receive " + message + " from " + address);
    }

    private void logFailPublishing(String address, String message) {
        log.debug("receive error " + message + " from " + address);
    }

    private JsonObject getRequestContent(RoutingContext context) {
        String contentType = context.request().headers().get(HttpHeaders.CONTENT_TYPE);

        if (contentType.toLowerCase().contains("multipart/form-data")) {
            JsonObject requestParameter = new JsonObject();
            context.request().formAttributes().forEach(entry -> requestParameter.put(entry.getKey(), entry.getValue()));
            return requestParameter;
        } else if (contentType.toLowerCase().contains("application/json")) {
            return context.getBody().toJsonObject();
        } else {
            return new JsonObject();
        }
    }
}

