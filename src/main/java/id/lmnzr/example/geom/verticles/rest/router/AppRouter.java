package id.lmnzr.example.geom.verticles.rest.router;


import id.lmnzr.example.geom.verticles.geometry.controller.GeometryController;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;

/**
 * The type App router.
 */
public final class AppRouter {
    private final Vertx vertx;

    /**
     * Instantiates a new App router.
     *
     * @param vertx the vertx
     */
    public AppRouter(Vertx vertx) {
        this.vertx = vertx;
    }

    /**
     * Gets router.
     *
     * @return the router
     */
    public Router getRouter() {
        Router router = Router.router(vertx);
        setCors(router);

        router.route().handler(BodyHandler.create());
        router.get("/").handler(this::indexHandler);
        router.route("/*").handler(StaticHandler.create());

        Router geomRouter = new GeometryController(vertx).getRouter();
        router.mountSubRouter("/geometry", geomRouter);

        return router;
    }

    private void indexHandler(RoutingContext context) {
        context.response()
            .putHeader("content-type", "text/plain")
            .end("Hello from Vert.x Geometry App!");
    }

    /**
     * Sets cors.
     *
     * @param router the router
     */
    public static void setCors(Router router) {
        router.route().handler(CorsHandler.create("*")
            .allowedMethod(HttpMethod.GET)
            .allowedMethod(HttpMethod.POST)
            .allowedMethod(HttpMethod.OPTIONS)
            .allowedHeader("Access-Control-Request-Method")
            .allowedHeader("Access-Control-Allow-Credentials")
            .allowedHeader("Access-Control-Allow-Origin")
            .allowedHeader("Access-Control-Allow-Headers")
            .allowedHeader("Access-Control-Request-Headers")
            .allowedHeader("Authorization")
            .allowedHeader("Content-Type"));
    }
}
