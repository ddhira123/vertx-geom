package id.lmnzr.example.geom.database.query;

import io.vertx.core.json.JsonArray;

/**
 * SQL generator
 */
public abstract class Generator {
    /**
     * The Table name.
     */
    protected String tableName;

    /**
     * The condition / where string
     */
    protected final StringBuilder where;
    /**
     * The Query parameter.
     */
    protected JsonArray queryParameter;

    /**
     * Instantiates a new Generator.
     *
     * @param tableName the table name
     */
    protected Generator(String tableName) {
        this.tableName = tableName;
        this.where = new StringBuilder();
        this.queryParameter = new JsonArray();
    }

    /**
     * Gets query parameter.
     *
     * @return the query parameter
     */
    public JsonArray getQueryParameter() {
        return queryParameter;
    }

    /**
     * Where equals generator.
     *
     * @param field the field
     * @param value the value
     */
    public void whereEquals(String field, Object value) {
        this.where.append(this.where.length() > 0 ? String.format(" %s ", WhereJoinOperator.AND.toString()) : "")
            .append(String.format("%s = ?", field));

        this.queryParameter.add(value);
    }

    /**
     * The enum Where join operator.
     */
    public enum WhereJoinOperator {
        /**
         * where join operator AND
         */
        AND,
        /**
         * where join operator OR
         */
        OR;

        WhereJoinOperator() {
        }
    }
}

