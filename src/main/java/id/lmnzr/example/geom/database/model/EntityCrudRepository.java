package id.lmnzr.example.geom.database.model;

import id.lmnzr.example.geom.database.DbJDBCResultSet;
import id.lmnzr.example.geom.utils.DateUtils;
import id.lmnzr.example.geom.utils.PojoMapper;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Entity crud repository.
 */
public abstract class EntityCrudRepository extends EntityRepository {
    /**
     * The constant ID.
     */
    public static final String ID = "id";
    /**
     * The constant CREATED_TIME.
     */
    public static final String CREATED_TIME = "created_time";
    /**
     * The constant LAST_MODIFIED_TIME.
     */
    public static final String LAST_MODIFIED_TIME = "last_modified_time";
    /**
     * The constant DELETED_TIME.
     */
    public static final String DELETED_TIME = "deleted_time";
    /**
     * The constant DELETED.
     */
    public static final String DELETED = "is_deleted";

    /**
     * Instantiates a new Master model.
     *
     * @param jdbcClient the jdbc client
     */
    public EntityCrudRepository(JDBCClient jdbcClient) {
        super(jdbcClient);
    }

    /**
     * Delete data by id.
     *
     * @param id                 the id
     * @param asyncResultHandler the async result handler
     */
    public void deleteById(Long id, Handler<AsyncResult<Boolean>> asyncResultHandler) {
        Map<String, Object> fieldToUpdate = new HashMap<String, Object>() {{
            put(DELETED, 1);
            put(DELETED_TIME, DateUtils.formatMySql(DateUtils.now()));
        }};

        this.update(fieldToUpdate, new HashMap<String, Object>() {{
            put(ID, id);
        }}, updateHandler -> DbJDBCResultSet.isUpdateSucceed(updateHandler, asyncResultHandler));
    }

    /**
     * Find data by ID field
     *
     * @param <T>                the type parameter
     * @param id                 the id
     * @param pojoClass          the pojo class
     * @param asyncResultHandler the async result handler
     */
    public <T> void findById(Long id, Class<T> pojoClass, Handler<AsyncResult<T>> asyncResultHandler) {
        Map<String,Object> filter = new HashMap<>();
        filter.put(ID,id);
        this.findByMultipleKey(filter, findHandler -> DbJDBCResultSet.toPojo(findHandler, pojoClass, asyncResultHandler));
    }

    /**
     * Insert new data
     *
     * @param <T>                the type parameter
     * @param data               the data
     * @param asyncResultHandler the async result handler
     */
    public <T> void insert(T data, Handler<AsyncResult<Long>> asyncResultHandler) {
        JsonObject dataJson = PojoMapper.toJsonObject(data, true);
        dataJson.put(CREATED_TIME, DateUtils.formatMySql(DateUtils.now()));

        super.insert(dataJson, insertHandler -> DbJDBCResultSet.getGeneratedKey(insertHandler, generatedKeyHandler -> {
            if (generatedKeyHandler.succeeded()) {
                asyncResultHandler.handle(Future.succeededFuture(generatedKeyHandler.result()));
            } else {
                asyncResultHandler.handle(Future.failedFuture(generatedKeyHandler.cause()));
            }
        }));
    }

    /**
     * Update data
     *
     * @param <T>                the type parameter
     * @param data               the data
     * @param asyncResultHandler the async result handler
     */
    public <T> void update(T data, Handler<AsyncResult<Boolean>> asyncResultHandler) {
        Map<String, Object> fieldToUpdate = PojoMapper.toHashMap(data, true);

        Long id = Long.valueOf(String.valueOf(fieldToUpdate.get(ID)));

        fieldToUpdate.remove(ID);
        fieldToUpdate.put(LAST_MODIFIED_TIME, DateUtils.formatMySql(DateUtils.now()));

        this.update(fieldToUpdate, new HashMap<String, Object>() {{
            put(ID, id);
        }}, updateHandler -> DbJDBCResultSet.isUpdateSucceed(updateHandler, asyncResultHandler));
    }
}
