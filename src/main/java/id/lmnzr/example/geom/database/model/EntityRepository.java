package id.lmnzr.example.geom.database.model;

import id.lmnzr.example.geom.database.exception.DatabaseException;
import id.lmnzr.example.geom.database.query.InsertQueryGenerator;
import id.lmnzr.example.geom.database.query.SelectQueryGenerator;
import id.lmnzr.example.geom.database.query.UpdateQueryGenerator;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.UpdateResult;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Entity repository.
 */
public abstract class EntityRepository {
    /**
     * The Jdbc client.
     */
    private final JDBCClient jdbcClient;

    /**
     * The Table name.
     */
    protected String tableName = "";

    /**
     * Instantiates a new Model.
     *
     * @param jdbcClient the jdbc client
     */
    public EntityRepository(JDBCClient jdbcClient) {
        this.jdbcClient = jdbcClient;
    }

    /**
     * Run select query
     *
     * @param sql     the select sql
     * @param handler the async result handler
     */
    protected void executeQuery(String sql, Handler<AsyncResult<ResultSet>> handler) {
        this.jdbcClient.query(sql, handler);
    }

    /**
     * Run select query with params.
     *
     * @param sql     the sql
     * @param params  the params
     * @param handler the async result handler
     */
    protected void executeQueryWithParams(String sql, JsonArray params, Handler<AsyncResult<ResultSet>> handler) {
        this.jdbcClient.queryWithParams(sql, params, handler);
    }

    /**
     * Run update, insert or delete query
     *
     * @param sql           the sql
     * @param resultHandler the async result handler
     */
    protected void executeUpdate(String sql, Handler<AsyncResult<UpdateResult>> resultHandler) {
        this.jdbcClient.update(sql, resultHandler);
    }

    /**
     * Run update, insert or delete query with params.
     *
     * @param sql     the sql
     * @param params  the params
     * @param handler the async result handler
     */
    protected void executeUpdateWithParams(String sql, JsonArray params, Handler<AsyncResult<UpdateResult>> handler) {
        this.jdbcClient.updateWithParams(sql, params, handler);
    }

    /**
     * Select row(s) with given condition string(s) if needed
     *
     * @param handler the async result handler
     */
    protected void find(Handler<AsyncResult<ResultSet>> handler) {
        executeFind(new HashMap<>(), null, null, handler);
    }

    /**
     * Select row(s) with given condition string(s) if needed
     *
     * @param limit   the limit
     * @param offset  the offset
     * @param handler the async result handler
     */
    protected void find(Integer limit, Integer offset, Handler<AsyncResult<ResultSet>> handler) {
        executeFind(new HashMap<>(), limit, offset, handler);
    }

    /**
     * Select row(s) by its key field value
     *
     * @param filter  the filter
     * @param handler the async result handler
     */
    protected void findByMultipleKey(Map<String, Object> filter, Handler<AsyncResult<ResultSet>> handler) {
        executeFind(filter,
            null,
            null,
            handler);
    }

    /**
     * Find by key.
     *
     * @param filter  the filter
     * @param limit   the limit
     * @param offset  the offset
     * @param handler the handler
     */
    protected void findByMultipleKey(Map<String, Object> filter, Integer limit, Integer offset, Handler<AsyncResult<ResultSet>> handler) {
        executeFind(filter,
            limit,
            offset,
            handler);
    }

    /**
     * Update data in database
     *
     * @param fieldToUpdate the fields name and values to update
     * @param conditionMap  the condition map
     * @param handler       the async result handler
     */
    protected void update(Map<String, Object> fieldToUpdate, Map<String, Object> conditionMap, Handler<AsyncResult<UpdateResult>> handler) {
        if (fieldToUpdate.isEmpty()) {
            handler.handle(Future.failedFuture(new DatabaseException("Invalid Parameter")));
        }

        UpdateQueryGenerator updateQueryGenerator = new UpdateQueryGenerator(this.tableName);

        fieldToUpdate.entrySet().iterator().forEachRemaining(objectEntry -> updateQueryGenerator.setFieldValue(objectEntry.getKey(), objectEntry.getValue()));

        conditionMap.entrySet().iterator().forEachRemaining(conditionEntry -> updateQueryGenerator.whereEquals(conditionEntry.getKey(), conditionEntry.getValue()));

        this.executeUpdateWithParams(updateQueryGenerator.generate(), updateQueryGenerator.getQueryParameter(), handler);
    }

    /**
     * Insert new row to database
     *
     * @param data    the data to insert
     * @param handler the async result handler
     */
    protected void insert(JsonObject data, Handler<AsyncResult<UpdateResult>> handler) {
        InsertQueryGenerator insertQueryGenerator = new InsertQueryGenerator(this.tableName);

        data.iterator().forEachRemaining(objectEntry -> insertQueryGenerator.addFieldValue(objectEntry.getKey(), objectEntry.getValue()));

        this.executeUpdateWithParams(insertQueryGenerator.generate(), insertQueryGenerator.getQueryParameter(), handler);
    }

    private void executeFind(Map<String, Object> filter, Integer limit, Integer offset, Handler<AsyncResult<ResultSet>> handler) {
        SelectQueryGenerator selectQueryGenerator = new SelectQueryGenerator();
        selectQueryGenerator.from(this.tableName);

        filter.entrySet().iterator().forEachRemaining(filterEntry -> selectQueryGenerator.whereEquals(filterEntry.getKey(), filterEntry.getValue()));

        if (limit != null) {
            selectQueryGenerator.limit(limit);
        }
        if (offset != null) {
            selectQueryGenerator.offset(offset);
        }

        this.executeQueryWithParams(selectQueryGenerator.generate(), selectQueryGenerator.getQueryParameter(), handler);
    }
}
