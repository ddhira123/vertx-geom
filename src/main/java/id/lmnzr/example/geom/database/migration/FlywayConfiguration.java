package id.lmnzr.example.geom.database.migration;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Flyway configuration class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FlywayConfiguration {
    /**
     * The constant MIGRATION_FILE_LOCATION.
     */
    public static final String MIGRATION_FILE_LOCATION = "database/migration";
    /**
     * The constant MIGRATION_TABLE_NAME.
     */
    public static final String MIGRATION_TABLE_NAME = "flyway_schema_history";
    /**
     * The constant FLYWAY_URL.
     */
    public static final String FLYWAY_URL = "flyway.url";
    /**
     * The constant FLYWAY_USER.
     */
    public static final String FLYWAY_USER = "flyway.user";
    /**
     * The constant FLYWAY_PASSWORD.
     */
    public static final String FLYWAY_PASSWORD = "flyway.password";
    /**
     * The constant FLYWAY_BASELINE_ON_MIGRATE.
     */
    public static final String FLYWAY_BASELINE_ON_MIGRATE = "flyway.baselineOnMigrate";
    /**
     * The constant FLYWAY_BASELINE_VERSION.
     */
    public static final String FLYWAY_BASELINE_VERSION = "flyway.baselineVersion";
    /**
     * The constant FLYWAY_LOCATION.
     */
    public static final String FLYWAY_LOCATION = "flyway.locations";
    /**
     * The constant FLYWAY_TABLE.
     */
    public static final String FLYWAY_TABLE = "flyway.table";

    /**
     * Gets configuration.
     *
     * @param uri the uri
     * @return the configuration
     */
    public static Map<String, String> getConfiguration(URI uri) {
        String url = String.format("jdbc:%s://%s:%d%s?useLegacyDatetimeCode=false&serverTimezone=Asia/Jakarta", uri.getScheme(),
            uri.getHost(), uri.getPort(), uri.getPath());
        String[] userInfo = uri.getUserInfo().split(":");

        Map<String, String> config = new HashMap<>();
        config.put(FLYWAY_URL, url);
        config.put(FLYWAY_USER, userInfo[0]);
        config.put(FLYWAY_PASSWORD, userInfo.length > 1 ? userInfo[1] : "");
        config.put(FLYWAY_BASELINE_ON_MIGRATE, "true");
        config.put(FLYWAY_BASELINE_VERSION, "0");
        config.put(FLYWAY_LOCATION, MIGRATION_FILE_LOCATION);
        config.put(FLYWAY_TABLE, MIGRATION_TABLE_NAME);

        return config;
    }
}
