package id.lmnzr.example.geom;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class MainTest {
    private static WebClient webClient;

    @BeforeClass
    public static void beforeClass(TestContext context) {
        Async async = context.async();
        Vertx vertx = Vertx.vertx();

        Main.main(null);

        webClient = WebClient.create(vertx);
        vertx.setTimer(2000, handler -> {
            async.complete();
        });
    }

    @Test
    public void welcomeRouteTest(TestContext context) {
        Async async = context.async();

        webClient
            .get(8585, "localhost", "/")
            .send(ar -> {
                HttpResponse<Buffer> response = ar.result();
                String responseWord = response.bodyAsString();
                context.assertEquals("Hello from Vert.x Geometry App!",responseWord);
                async.complete();
            });
    }

}