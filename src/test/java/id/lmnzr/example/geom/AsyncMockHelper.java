package id.lmnzr.example.geom;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.UpdateResult;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

public final class AsyncMockHelper {
    public final static String ERROR_ASYNC_UPDATE = "Error Updating";
    public final static String ERROR_ASYNC_QUERY= "Error Querying";
    public final static String ERROR_ASYNC_NOT_FOUND= "Data Not Found";

    public static void mockSuccesfulUpdate(Object expectedResult, int numberUpdated, JDBCClient jdbcClient) {
        AsyncResult<UpdateResult> asyncResultUpdateResult = mockAsyncUpdateResultSuccess(expectedResult, numberUpdated);
        mockAsyncUpdateResultHandler(asyncResultUpdateResult, jdbcClient);
    }

    public static void mockFailedUpdate(JDBCClient jdbcClient) {
        AsyncResult<UpdateResult> asyncResultUpdateResult = mockAsyncUpdateResultFailed(null, 0);
        mockAsyncUpdateResultHandler(asyncResultUpdateResult, jdbcClient);
    }

    public static void mockSuccesfulQuery(List<JsonObject> expectedResults, JDBCClient jdbcClient) {
        AsyncResult<ResultSet> asyncQueryResult = mockAsyncResultSetSuccess(expectedResults);
        mockAsyncQueryResultHandler(asyncQueryResult, jdbcClient);
    }

    public static void mockSuccesfulQuery(JsonObject expectedResult, JDBCClient jdbcClient) {
        List<JsonObject> expectedResults = new ArrayList<>();
        expectedResults.add(expectedResult);
        AsyncResult<ResultSet> asyncQueryResult = mockAsyncResultSetSuccess(expectedResults);
        mockAsyncQueryResultHandler(asyncQueryResult, jdbcClient);
    }

    public static void mockFailedQuery(JDBCClient jdbcClient) {
        AsyncResult<ResultSet> asyncQueryResult = mockAsyncResultSetFailed(new ArrayList<>());
        mockAsyncQueryResultHandler(asyncQueryResult, jdbcClient);
    }

    public static void mockNotFoundQuery(JDBCClient jdbcClient) {
        AsyncResult<ResultSet> asyncQueryResult = mockAsyncResultSetNotFound(new ArrayList<>());
        mockAsyncQueryResultHandler(asyncQueryResult, jdbcClient);
    }

    private static UpdateResult mockUpdateResult(Object expectedResult, int numberUpdated) {
        UpdateResult res = Mockito.mock(UpdateResult.class);
        JsonArray jsonArray = new JsonArray();
        jsonArray.add(expectedResult);
        Mockito.when(res.getKeys()).thenReturn(jsonArray);
        Mockito.when(res.getUpdated()).thenReturn(numberUpdated);
        return res;
    }

    private static ResultSet mockResultSet(List<JsonObject> expectedResults) {
        ResultSet res = Mockito.mock(ResultSet.class);
        Mockito.when(res.getNumRows()).thenReturn(expectedResults.size());
        Mockito.when(res.getRows()).thenReturn(expectedResults);
        return res;
    }

    private static AsyncResult<UpdateResult> mockAsyncUpdateResultSuccess(Object expectedResult, int numberUpdated) {
        UpdateResult res = mockUpdateResult(expectedResult, numberUpdated);
        AsyncResult<UpdateResult> asyncResultUpdateResult = Mockito.mock(AsyncResult.class);
        Mockito.when(asyncResultUpdateResult.succeeded()).thenReturn(true);
        Mockito.when(asyncResultUpdateResult.result()).thenReturn(res);

        return asyncResultUpdateResult;
    }

    private static AsyncResult<UpdateResult> mockAsyncUpdateResultFailed(Object expectedResult, int numberUpdated) {
        mockUpdateResult(expectedResult, numberUpdated);
        AsyncResult<UpdateResult> asyncResultUpdateResult = Mockito.mock(AsyncResult.class);
        Mockito.when(asyncResultUpdateResult.succeeded()).thenReturn(false);
        Mockito.when(asyncResultUpdateResult.cause()).thenReturn(new Exception(ERROR_ASYNC_UPDATE));

        return asyncResultUpdateResult;
    }

    private static AsyncResult<ResultSet> mockAsyncResultSetSuccess(List<JsonObject> expectedResults) {
        ResultSet res = mockResultSet(expectedResults);
        AsyncResult<ResultSet> asyncResultResultSet = Mockito.mock(AsyncResult.class);
        Mockito.when(asyncResultResultSet.succeeded()).thenReturn(true);
        Mockito.when(asyncResultResultSet.result()).thenReturn(res);

        return asyncResultResultSet;
    }

    private static AsyncResult<ResultSet> mockAsyncResultSetFailed(List<JsonObject> expectedResults) {
        mockResultSet(expectedResults);
        AsyncResult<ResultSet> asyncResultResultSet = Mockito.mock(AsyncResult.class);
        Mockito.when(asyncResultResultSet.succeeded()).thenReturn(false);
        Mockito.when(asyncResultResultSet.cause()).thenReturn(new Exception(ERROR_ASYNC_QUERY));

        return asyncResultResultSet;
    }

    private static AsyncResult<ResultSet> mockAsyncResultSetNotFound(List<JsonObject> expectedResults) {
        mockResultSet(expectedResults);
        AsyncResult<ResultSet> asyncResultResultSet = Mockito.mock(AsyncResult.class);
        Mockito.when(asyncResultResultSet.succeeded()).thenReturn(false);
        Mockito.when(asyncResultResultSet.cause()).thenReturn(new Exception(ERROR_ASYNC_NOT_FOUND));

        return asyncResultResultSet;
    }

    private static void mockAsyncUpdateResultHandler(AsyncResult<UpdateResult> asyncResultUpdateResult, JDBCClient jdbcClient) {
        Mockito.doAnswer((Answer<AsyncResult<UpdateResult>>) arg0 -> {
            ((Handler<AsyncResult<UpdateResult>>) arg0.getArgument(2)).handle(asyncResultUpdateResult);
            return null;
        }).when(jdbcClient).updateWithParams(Mockito.any(), Mockito.any(), Mockito.any());
    }

    private static void mockAsyncQueryResultHandler(AsyncResult<ResultSet> asyncResultQueryResult, JDBCClient jdbcClient) {
        Mockito.doAnswer((Answer<AsyncResult<ResultSet>>) arg0 -> {
            ((Handler<AsyncResult<ResultSet>>) arg0.getArgument(2)).handle(asyncResultQueryResult);
            return null;
        }).when(jdbcClient).queryWithParams(Mockito.any(), Mockito.any(), Mockito.any());
    }
}
