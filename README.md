# Vertx Geom

[![pipeline status](https://gitlab.com/almas.hilman/vertx-geom/badges/master/pipeline.svg)](https://gitlab.com/almas.hilman/vertx-geom/commits/master) 
[![coverage report](https://gitlab.com/almas.hilman/vertx-geom/badges/master/coverage.svg)](https://almas.hilman.gitlab.io/vertx-geom/coverage/)

* [Description](#description)
* [Requirement](#requirement)
* [Pipeline](#pipeline)
* [Installation](#installation)
* [Usage](#usage)
* [Documentation](#documentation)

## Description

In this project I build a simple REST API using [Vert.x](https://vertx.io/).
The Main objective of this project is to build an example of asynchronous application using Vert.x Java.
To achieve that, I implement [Verticles](https://vertx.io/docs/vertx-core/java/#_verticles) and [EventBus](https://vertx.io/docs/vertx-core/java/#event_bus) of Vert.x toolkit.
The project used Java 8 as the programming language and MariaDB database to persist data.
With geometry API you can retrieve saved geometry or save your own geometry in the database.

## Requirement
To use the application it is required to use:
+ [Java 8](https://java.com/en/download/faq/java8.xml)
+ [MariaDB](https://mariadb.org/)

## Pipeline
The CI/CD pipeline is run using [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/). 
It consists of two stage:
+ Test phase where the code is build then tested using maven command.
+ Deploy phase where the application deployed to [Heroku](https://www.heroku.com/) and documentation deployed to [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/).

## Installation
### Setup
You can set environment variable to fit with your own environment:
+ `PORT` : The port where application run (default : 8585)
+ `GEOM_DB` : The database connection string (default: "mysql://root:root@localhost:3306/test")

### Package
You can build, run test, and package your application into a fat JAR using maven package command.
```
mvn package
```

### Run
Then using java command you can run the generated fat JAR in the target folder.
```
java -jar target/vertxgeom-{version}-fat.jar 
```

### Cleanup
You can cleanup generated files from package using maven clean command.
```
mvn clean
```

## Usage

Usage documentation can be found [here](https://vertxgeom.herokuapp.com/swagger-ui/)

## Documentation

Further documentation can be found [here](https://almas.hilman.gitlab.io/vertx-geom/)
